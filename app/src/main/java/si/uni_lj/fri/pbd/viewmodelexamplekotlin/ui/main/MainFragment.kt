package si.uni_lj.fri.pbd.viewmodelexamplekotlin.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import si.uni_lj.fri.pbd.viewmodelexamplekotlin.R
import si.uni_lj.fri.pbd.viewmodelexamplekotlin.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = MainFragment()
        private val usd_to_eu_rate = 0.92f
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.convertButton.setOnClickListener {
            if (binding.dollarText.text.toString().toFloatOrNull() != null){
                binding.resultText.text = (binding.dollarText.text.toString().toFloat() * usd_to_eu_rate).toString()

            } else {
                binding.resultText.text = getString(R.string.no_value)
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}